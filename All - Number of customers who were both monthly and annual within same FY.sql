
;
-- how many monthly were active
select count(distinct sen)
from public.sale
where financial_year = 'FY2016'
and hosted_monthly = true
and platform = 'Cloud'
and base_product <> 'Marketplace Addon'
and license_level <> 'Starter'

;
-- final version
with total as 
(
select distinct sen, max(date) as date
from public.sale
where financial_year = 'FY2016'
and hosted_monthly = true
and platform = 'Cloud'
and base_product <> 'Marketplace Addon'
and license_level <> 'Starter'
group by 1
),
annual as 
(
select distinct sen, min(date) as date
from public.sale
where financial_year = 'FY2016'
and hosted_monthly = false
and platform = 'Cloud'
and base_product <> 'Marketplace Addon'
and license_level <> 'Starter'
group by 1
),
date_check as
(
select distinct a.sen, a.date, b.date
from annual as a 
left join total as b on a.sen = b.sen
where b.date is not null
and b.date < a.date
)
select count(distinct sen)
from date_check