select  user_limit,
        sum(amount), 
        count(distinct email_domain), 
        count(distinct sen)
from public.sale 
where platform = 'Cloud'
and base_product = 'Bitbucket'
and financial_year = 'FY2016'
and hosted_annual = true
group by 1
