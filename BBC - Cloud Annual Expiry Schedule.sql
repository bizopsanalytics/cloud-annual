with bb_paid as
 (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                c.period_effective_end_date,
                c.period_end_date,        
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')  
        and     c.billing_period = 'Annual'
        and     b.platform = 'Cloud'
        and     b.base_product in ('Bitbucket')  
        )
       
        select  year(period_end_date),
                month(period_end_date), 
                unit_count,
                count(distinct sen) as counter
        from bb_paid
        group by 1,2,3
        order by 1,2,3
     