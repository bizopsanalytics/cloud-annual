
        
        select sen, billing_period, min(period_created_date)
        from model.dim_license
        where billing_period <> 'Other'
        group by 1,2
        order by 1,2
        
       ;
       with annual_check as
       (
       select sen, 
                case when billing_period = 'Monthly' then period_created_date end as "1",
                case when billing_period = 'Annual' then period_created_date end as "2"
        from model.dim_license
        where description like '%Bitbucket (Cloud)%'
        and substring(cast(period_created_date as varchar),1,10) > '2016-01-01'
        and level in ('Full', 'Starter')
        group by 1,2,3
        order by 1     
        )
        select b.tech_email_domain, a.* 
        from annual_check as a
        left join public.license as b on a.sen = b.sen
        where "2" is not null
        group by 1,2,3,4
        order by 1,2,3,4
        ;
        
        with dates as
        (
        select  email_domain, 
                sold_via_partner, 
                case when hosted_annual = false then date end as "monthly",
                case when hosted_annual = true then date end as "annual"              
        from public.sale
        where base_product = 'Bitbucket'
        and platform = 'Cloud'
        and financial_year in ('FY2015','FY2016')
        group by 1,2,3,4
        order by 1,2,3,4
        )
        select email_domain, sold_via_partner, min(monthly), min(annual)
        from dates       
        --where annual is not null
        group by 1,2
        order by 1,2
        
        
   